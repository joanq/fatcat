# Frequency Analysis Test - Catch All Tones

`fatcat` is a simple program that reads a collection of one-shot samples
(samples that contain a single note) and tries to guess the note and octave of
each one.

To make its guesses, `fatcat` analyzes the frequencies present in the sounds,
and applies some statistics and heuristics. It will provide up to three
candidates for each sample, ordered by likeliness.

If happy with the results, you can also tell `fatcat` to rename all samples,
adding the best candidates note name to the end of the file name.

`fatcat` is based on the ideas by Ian Vonseggern in
https://github.com/ianvonseggern1/note-prediction.
