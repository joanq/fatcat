#!/usr/bin/env python3

import argparse
import array
import numpy as np
import scipy.fft
import os

from pydub import AudioSegment
from collections import Counter
from pydub.utils import get_array_type
from bisect import bisect_left

NOTES = {
    "C": 261.6256,
    "C#": 277.1826,
    "D": 293.6648,
    "D#": 311.1270,
    "E": 329.6276,
    "F": 349.2282,
    "F#": 369.9944,
    "G": 391.9954,
    "G#": 415.3047,
    "A": 440,
    "A#": 466.1637615180899,
    "B": 493.8833012561241
}

NOTE_NAMES=["C","C#","D","D#","E","F","F#","G","G#","A","A#","B"]
MAX_FREQUENCY=8000
MIN_MAGNITUDE=0.001

verbosity=0
min_octave=0
max_octave=8
tolerance=33

def main(files, rename=False):
    if len(files)==0:
        print(" /")
        print("(")
        print(" )    /\/\\")
        print("(  __(  ··)")
        print(" \(      /")
        print("   \____/")
        print("     ^ ^")
    for file in files:
        sample = AudioSegment.from_file(file)
        if (verbosity>0):
            print()
            print(f"---- {file} ----")
        notes, octaves = predict_notes(sample)
        print(f"{file} ", end="")
        for i in range(len(notes)):
            print(f"{notes[i]}{octaves[i]} ", end="")
        print()
        if rename and len(notes)>0:
            path, extension = os.path.splitext(file)
            os.rename(file, path+"-"+notes[0]+str(octaves[0])+extension)

def predict_notes(sample):
    octaves=[]
    freqs, freq_magnitudes = frequency_spectrum(sample)
    notes = classify_note(freqs, freq_magnitudes)
    for note in notes:
        octaves.append(classify_octave(freqs, freq_magnitudes, note))
    return notes, octaves

def frequency_spectrum(sample):
    """
    Derive frequency spectrum of a signal pydub.AudioSample
    Returns an array of frequencies and an array of how prevelant that frequency is in the sample
    """
    # Convert pydub.AudioSample to raw audio data
    # Copied from Jiaaro's answer on https://stackoverflow.com/questions/32373996/pydub-raw-audio-data
    bit_depth = sample.sample_width * 8
    array_type = get_array_type(bit_depth)
    raw_audio_data = array.array(array_type, sample._data)
    freq_array=[]
    freq_magnitude=[]
    n = len(raw_audio_data)
    if n>0:
        # Compute FFT and frequency value for each index in FFT array
        # Inspired by Reveille's answer on https://stackoverflow.com/questions/53308674/audio-frequencies-in-python
        freq_array = np.arange(n) * (float(sample.frame_rate) / n)  # two sides frequency range
        freq_array = freq_array[: (n // 2)]  # one side frequency range

        raw_audio_data = raw_audio_data - np.average(raw_audio_data)  # zero-centering
        freq_magnitude = scipy.fft.fft(raw_audio_data)  # fft computing and normalization
        freq_magnitude = freq_magnitude[: (n // 2)]  # one side

        freq_magnitude = abs(freq_magnitude)
        freq_magnitude = freq_magnitude / np.sum(freq_magnitude)

        cutpoint = bisect_left(freq_array, MAX_FREQUENCY)
        freq_array = freq_array[0:cutpoint]
        freq_magnitude = freq_magnitude[0:cutpoint]
    return freq_array, freq_magnitude

def classify_octave(freq_array, freq_magnitude, predicted):
    oct_counter = Counter()
    oct=10
    for i in range(len(freq_magnitude)):
        if freq_magnitude[i] < MIN_MAGNITUDE:
            continue
        note, octave = get_note_for_freq(freq_array[i])
        if note==predicted:
            oct_counter[octave] += freq_magnitude[i]
    avg = oct_counter.total()/len(oct_counter)
    for elem in oct_counter:
        if elem<oct and oct_counter[elem]>=avg:
            oct=elem
    return oct

def classify_note(freq_array, freq_magnitude):
    note_counter = Counter()
    max_freq=0
    max_note='U'
    for i in range(len(freq_magnitude)):
        if freq_magnitude[i] < MIN_MAGNITUDE:
            continue
        note, octave = get_note_for_freq(freq_array[i])
        if note:
            note_counter[note] += freq_magnitude[i]
            if freq_magnitude[i]>max_freq:
                max_freq=freq_magnitude[i]
                max_note=note
        if verbosity>1:
            print(f'{freq_array[i]} {freq_magnitude[i]} {note}{octave}')
    if verbosity>1:
        print(note_counter.most_common(12))
    elif verbosity>0:
        print(note_counter.most_common(3))

    notes=[]
    n = note_counter.most_common(3)
    if len(n)==0:
        pass
    elif len(n)==1 or n[0][1]>n[1][1]*2:
        # We can be quite sure about this note
        notes.append(n[0][0])
    elif len(n)==2 or n[0][1]>n[2][1]*2:
        # Two candidates
        # Find if one note is a fifth over the other
        if fifth(n[1][0], n[0][0]):
            notes.append(n[0][0])
            notes.append(n[1][0])
        elif fifth(n[0][0], n[1][0]):
            notes.append(n[1][0])
            notes.append(n[0][0])
        # If freqs are similar, get the maximum peak first
        elif n[1][0]==max_note:
            notes.append(n[1][0])
            notes.append(n[0][0])
        else:
            notes.append(n[0][0])
            notes.append(n[1][0])
    else: # len(n)==3
        # Three candidates
        # Find if one note is a fifth over the other
        if fifth(n[1][0], n[0][0]):
            notes.append(n[0][0])
            notes.append(n[1][0])
            notes.append(n[2][0])
        elif fifth(n[0][0], n[1][0]):
            notes.append(n[1][0])
            notes.append(n[0][0])
            notes.append(n[2][0])
        elif fifth(n[2][0], n[0][0]):
            notes.append(n[0][0])
            notes.append(n[2][0])
            notes.append(n[1][0])
        elif fifth(n[0][0], n[2][0]):
            notes.append(n[2][0])
            notes.append(n[0][0])
            notes.append(n[1][0])
        elif fifth(n[2][0], n[1][0]):
            notes.append(n[1][0])
            notes.append(n[2][0])
            notes.append(n[0][0])
        elif fifth(n[1][0], n[2][0]):
            notes.append(n[2][0])
            notes.append(n[1][0])
            notes.append(n[0][0])
        elif n[1][0]==max_note:
            # If freqs are similar, get the maximum peak first
            notes.append(n[1][0])
            notes.append(n[0][0])
            notes.append(n[2][0])
        elif n[2][0]==max_note:
            notes.append(n[2][0])
            notes.append(n[0][0])
            notes.append(n[1][0])
        else:
            notes.append(n[0][0])
            notes.append(n[1][0])
            notes.append(n[2][0])
    return notes

def fifth(note1, note2):
    note1pos=NOTE_NAMES.index(note1)
    note2pos=NOTE_NAMES.index(note2)
    if (note2pos+7)%12==note1pos:
        return True
    return False

# If f is within tolerance of a note (measured in cents - 1/100th of a semitone)
# return that note, otherwise returns None
# We scale to the 4th octave to check
def get_note_for_freq(f):
    # Calculate the range for each note
    tolerance_multiplier = 2 ** (tolerance / 1200)
    note_ranges = {
        k: (v / tolerance_multiplier, v * tolerance_multiplier) for (k, v) in NOTES.items()
    }

    # Get the frequence into the 4th octave
    range_min = note_ranges["C"][0]
    range_max = note_ranges["B"][1]
    octave = 4
    if f < range_min:
        while f < range_min:
            f *= 2
            octave-=1
    else:
        while f > range_max:
            f /= 2
            octave+=1

    # Check if any notes match
    if octave>=min_octave and octave<=max_octave:
        for (note, note_range) in note_ranges.items():
            if f > note_range[0] and f < note_range[1]:
                return note, octave
    return None, None

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Frequency Analysis Test - Catch All Tones: Guess the tone of one-shot samples.")
    parser.add_argument("files", nargs='*',
            help="Files to analyze.")
    parser.add_argument("-v", "--verbosity", action="count", default=0,
            help="Increase verbosity. Repeat two times for more.")
    parser.add_argument("-m", "--min-octave", type=int, default=0,
            help="Minimum octave to analyze, defaults to 0.")
    parser.add_argument("-x", "--max-octave", type=int, default=8,
            help="Maximum octave to analyze, defaults to 8.")
    parser.add_argument("-t", "--tolerance", type=int, default=33,
            help="Maximum frequency deviation from a note, in cents. Defaults to 33.")
    parser.add_argument("-r", "--rename", action="store_true", help="Add detected note to the file name.")
    args = parser.parse_args()
    verbosity=args.verbosity
    min_octave=args.min_octave
    max_octave=args.max_octave
    tolerance=args.tolerance
    main(args.files, args.rename)
